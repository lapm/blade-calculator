#!/usr/bin/env python
# coding: utf-8

# # Blade Calculator functions
# 
# José Alberto Navarro Martínez (s175511@student.dtu.dk), July 2019
# 
# Modified by Lars P. Mikkelsen, January 2020
# 
# This code has been developed as part of the final project "Material choice and blade design", for the Wind Energy Master at Technical University of Denmark (DTU).
# 
# Given a set of material(s) and blade length(s), it calculates the optimised blade mass, blade tip deflection, cost, embodied energy and CO2 production.

# In[1]:


# Import needed Python libraries
import numpy as np
from scipy.integrate import simps
from scipy.optimize import minimize
from scipy.optimize import curve_fit

# ## Defined functions

# ### Function `mass`
# 
# Calculates the mass of the load-carrying beam of the blade.
# 
# $$M = \rho_{mat}\int_0^L A_{blade}(x) \textrm{d} x$$

# In[2]:


# Function that calculates the mass of the blade
def mass(A_flap,A_edge,rho,x):    
    A_blade = 2*(A_flap+A_edge)
    mass = simps(A_blade,x)*rho    
    return mass/1000 # returns mass in tonnes


# ### Function `mass_jacob`
# 
# Calculates the Jacobian matrix of the function `mass`, which is useful for the SLSQP optimisation process.
# 
# According to the first fundamental theorem of calculus:
# 
# Let $f$ be a continuous real-valued function defined on a closed interval $[a,b]$.  Let $F$ be the function defined, for all $x$ in $[a,b]$, by
# 
# $$F(x) = \int_a^xf(t)dt$$
# 
# Then, $F$ is uniformly continuous on $[a,b]$, differentiable on the open interval $(a,b)$, and
# 
# $$F'(x) = f(x)$$
# 
# for all $x$ in $(a,b)$.
# 
# Consequently,
# 
# $$M'(x) = \rho_{mat}A_{blade}(x)$$

# In[3]:


# Function that calculates the Jacobian of the mass
def mass_jacob(A_flap,A_edge,rho,x):    
    A_blade = 2*(A_flap+A_edge)    
    return rho*A_blade


# ### Functions `flapmass` and `flapmass_jacob`
# 
# Similarly to `mass` and `mass_jacob` functions, they calculate the flap contribution of the load-carrying beam mass, and its Jacobian matrix:
# 
# $$Mass_{flap} = \rho_{mat}\int_0^L 2 A_{flap}(x) \textrm{d}x$$
# 
# $$Mass_{flap}'(x) = 2\rho_{mat}A_{flap}(x)$$

# In[4]:


# Function that calculates the flap mass of the blade
def flapmass(A_flap,rho,x):    
    flapmass = 2*simps(A_flap,x)*rho    
    return flapmass/1000 # returns mass in tonnes

# Function that calculates the Jacobian of the flap mass
def flapmass_jacob(A_flap,rho,x):    
    return 2*rho*A_flap


# ### Function `Medge`
# 
# Calculates the edgewise moment distribution along the blade.
# 
# $$M_{edge}(x) = \int_{x}^L q_{edge}(x')\cdot(x'-x)\textrm{d}x'$$

# In[5]:


# Function that calculates the edge-wise moment of the blade
def Medge(A_flap,A_edge,rho,g,x):    
    A_blade = 2*(A_flap+A_edge)
    q_edge = rho*g*A_blade    
    N = len(x)
    Medge = []
    for x0 in x:
        mom = (x-x0)*q_edge
        mom2 = np.zeros(N)
        for i in range(0,N):
            if mom[i] < 0:
                mom2[i] = 0
            else:
                mom2[i] = mom[i]
        momx0 = simps(mom2,x)
        Medge = np.append(Medge,momx0)        
    return Medge


# ### Function `sigma_edge`
# 
# Calculates the normal stress on the blade in the edgewise direction.
# 
# $$\sigma_{edge}(x) = \frac{M_{edge}(x)}{h_{edge}(x)A_{edge}(x)}$$

# In[6]:


# Function that calculates the edge-wise stress of the blade
def sigma_edge(A_flap,A_edge,rho,g,x,h_edge):    
    A_blade = 2*(A_flap+A_edge)
    q_edge = rho*g*A_blade    
    N = len(x)
    Medge = []
    for x0 in x:
        mom = (x-x0)*q_edge
        mom2 = np.zeros(N)
        for i in range(0,N):
            if mom[i] < 0:
                mom2[i] = 0
            else:
                mom2[i] = mom[i]
        momx0 = simps(mom2,x)
        Medge = np.append(Medge,momx0)        
    sigma_edge = Medge/(h_edge*A_edge)    
    return sigma_edge # in Pa


# ### Function `Mflap`
# 
# Calculates the flapwise moment distribution along the blade.
# 
# $$M_{flap}(x) = q_x\frac{(L-x)^2}{2} + (q_L-q_x)\frac{(L-x)^2}{3} = \frac{8}{81}\rho_{air}u_R^2\pi R^2L\left(1+\frac{x}{2L}\right)\left(1-\frac{x}{L}\right)^2$$

# In[7]:


# Function that calculates the flap-wise moment of the blade
def Mflap(A_flap,rho_air,uR,x,L,h_flap,R):    
    qx_flap = 8/27*rho_air*uR**2*np.pi*R**2*x/L**2 
    Mx_flap = qx_flap*(L-x)**2/2 + (qx_flap[-1]-qx_flap)*(L-x)**2/3    
    return Mx_flap


# ### Function `sigma_flap`
# 
# Calculates the normal stress on the blade in the flapwise direction.
# 
# $$\sigma_{flap}(x) = \frac{M_{flap}(x)}{h_{flap}(x)A_{flap}(x)}$$

# In[8]:


# Function that calculates the flap-wise stress of the blade
def sigma_flap(A_flap,rho_air,uR,x,L,h_flap,R):    
    qx_flap = 8/27*rho_air*uR**2*np.pi*R**2*x/L**2 
    Mx_flap = qx_flap*(L-x)**2/2 + (qx_flap[-1]-qx_flap)*(L-x)**2/3
    sigma_flap = Mx_flap/(A_flap*h_flap)    
    return sigma_flap # in Pa


# ### Function `tipdisp`
# 
# Calculates the blade tip displacement in flap direction, $w_{flap}(x=L)$.
# 
# The flap deflection of the blade is calculated using Bernouilli-Euler beam theory:
# 
# $$\frac{\textrm{d}^2w_{flap}(x)}{\textrm{d}x^2} = \frac{M_{flap}(x)}{EI_{flap}(x)}$$
# 
# where $I_{flap}(x) = \frac{1}{2}h_{flap}^2(x)A_{flap}(x)$
# 
# It can be solved using this numerical difference scheme:
# 
# $$\theta_i = \theta_{i-1} + \frac{1}{2}\left(\frac{M_i}{EI_i}+\frac{M_{i-1}}{EI_{i-1}}\right)(x_i-x_{i-1})$$
# 
# $$w_i = w_{i-1}+\frac{\theta_i + \theta_{i-1}}{2}(x_i-x_{i-1})$$
# 
# for i = 1,...,n.  
# 
# Boundary conditions: $\theta_0$ = 0, $w_0$ = 0

# In[9]:


# Function that calculates the blade tip displacement
def tipdisp(A_flap,rho_air,uR,x,L,E,h_flap,R):    
    N = len(x)    
    qx_flap = 8/27*rho_air*uR**2*np.pi*R**2*x/L**2 
    Mx_flap = qx_flap*(L-x)**2/2 + (qx_flap[-1]-qx_flap)*(L-x)**2/3
    EIx_flap = 1/2*A_flap*h_flap**2*E    
    Thx_flap = np.zeros(N) 
    for i in range(1,N):
        Thx_flap[i] = Thx_flap[i-1] + 0.5*(Mx_flap[i]/EIx_flap[i] + Mx_flap[i-1]/EIx_flap[i-1])*(x[i]-x[i-1])    
    w_flap = np.zeros(N)
    for i in range(1,N):
        w_flap[i] = w_flap[i-1] + 0.5*(Thx_flap[i]+Thx_flap[i-1])*(x[i]-x[i-1])
    tipdisp = max(w_flap)    
    return tipdisp # in m

